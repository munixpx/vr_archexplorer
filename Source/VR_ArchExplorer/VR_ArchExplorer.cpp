// Copyright Epic Games, Inc. All Rights Reserved.

#include "VR_ArchExplorer.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, VR_ArchExplorer, "VR_ArchExplorer" );
