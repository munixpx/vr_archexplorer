﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "VRPawn.h"

#include "DrawDebugHelpers.h"
#include "HeadMountedDisplayFunctionLibrary.h"
#include "NavigationSystem.h"
#include "Kismet/GameplayStatics.h"

// Sets default values
AVRPawn::AVRPawn()
{
	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	// Components setup
	VRRoot = CreateDefaultSubobject<USceneComponent>(TEXT("VRRoot"));
	VRRoot->SetupAttachment(GetRootComponent());

	CameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("CameraComponent"));
	CameraComponent->SetupAttachment(VRRoot);
	
	TeleportSplineComponent = CreateDefaultSubobject<USplineComponent>(TEXT("SplineTeleportPath"));
	TeleportSplineComponent->SetMobility(EComponentMobility::Movable);
	TeleportSplineComponent->SetupAttachment(VRRoot);
	
}

// Called when the game starts or when spawned
void AVRPawn::BeginPlay()
{
	Super::BeginPlay();

	bIsTeleporting = false;

	// Setting correct eye height
	if(UHeadMountedDisplayFunctionLibrary::IsHeadMountedDisplayEnabled())
	{
		UHeadMountedDisplayFunctionLibrary::SetTrackingOrigin(EHMDTrackingOrigin::Floor);
	}

	if(MotionControllerClass)
	{
		LeftMotionController = Cast<AHandController>(GetWorld()->SpawnActor<AHandController>(MotionControllerClass));
		if(LeftMotionController)
		{
			LeftMotionController->AttachToComponent(VRRoot, FAttachmentTransformRules::KeepRelativeTransform);
			LeftMotionController->SetOwner(this);
			LeftMotionController->SetTrackingSource(EControllerHand::Left);
		}

		RightMotionController = Cast<AHandController>(GetWorld()->SpawnActor<AHandController>(MotionControllerClass));
		if(RightMotionController)
		{
			RightMotionController->AttachToComponent(VRRoot, FAttachmentTransformRules::KeepRelativeTransform);
			RightMotionController->SetOwner(this);
			RightMotionController->SetTrackingSource(EControllerHand::Right);
		}
	}
}

// Called every frame
void AVRPawn::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	// ResetVRRoot();

	if (bIsTeleporting)
	{
		TraceMovement();
	}

	Move();
}

// Called to bind functionality to input
void AVRPawn::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
	
	PlayerInputComponent->BindAxis(TEXT("MoveForward"), this, &AVRPawn::CalculateMovementInput);
	PlayerInputComponent->BindAxis(TEXT("MoveRight"), this, &AVRPawn::MoveRight);
	PlayerInputComponent->BindAction(TEXT("Teleport"), IE_Pressed, this, &AVRPawn::BeginTeleport);
	PlayerInputComponent->BindAction(TEXT("Teleport"), IE_Released, this, &AVRPawn::Teleport);
	PlayerInputComponent->BindAction(TEXT("GrabLeft"), IE_Pressed, this, &AVRPawn::GrabLeftBegin);
	PlayerInputComponent->BindAction(TEXT("GrabLeft"), IE_Released, this, &AVRPawn::GrabLeftEnd);
	PlayerInputComponent->BindAction(TEXT("GrabRight"), IE_Pressed, this, &AVRPawn::GrabRightBegin);
	PlayerInputComponent->BindAction(TEXT("GrabRight"), IE_Released, this, &AVRPawn::GrabRightEnd);
}

void AVRPawn::ResetVRRoot()
{
	// Move Actor to match the Camera/Player Space and then reset the Camera space 
	FVector CameraOffset = CameraComponent->GetComponentLocation() - GetActorLocation();
	CameraOffset.Z = 0.0f;
	AddActorWorldOffset(CameraOffset);
	VRRoot->AddWorldOffset(-CameraOffset);
}

// TELEPORTATION
bool AVRPawn::TraceTeleportPath(FHitResult& OutHitResult, TArray<FVector>& OutPathLocations)
{
	const FVector StartLocation = LeftMotionController->GetActorLocation();
	FVector Direction = LeftMotionController->GetActorForwardVector();
	Direction = Direction.RotateAngleAxis(TraceArchAngleOffset * -1, LeftMotionController->GetActorRightVector());
	
	DrawDebugPoint(GetWorld(), StartLocation, 100, FColor::Blue);
	// run the tracing
	FPredictProjectilePathParams PathParams(
		TraceDebugRadius,
		StartLocation,
		Direction * TraceVelocity,
		TraceSimTime,
		ECC_Visibility,
		this);
	// PathParams.DrawDebugType = EDrawDebugTrace::ForOneFrame;
	FPredictProjectilePathResult PredictResult;
	bool bCollision = UGameplayStatics::PredictProjectilePath(GetWorld(), PathParams, PredictResult );
	 
	// prepare out data
	OutHitResult = PredictResult.HitResult;
	OutPathLocations.Reset();
	for( FPredictProjectilePathPointData PointData : PredictResult.PathData)
	{
		OutPathLocations.Add(PointData.Location);
	}
	return bCollision;
}

void AVRPawn::DrawTeleportSpline() const
{
	TeleportSplineComponent->ClearSplinePoints();
	TArray<FVector> LocalPoints;
	for(FVector x: TracePathLocations)
	{
		 LocalPoints.Add(VRRoot->GetComponentTransform().InverseTransformPosition(x));
	}
	TeleportSplineComponent->SetSplinePoints(LocalPoints, ESplineCoordinateSpace::Local);
}

void AVRPawn::DrawTeleportPath()
{
	const int32 NumExistingSegments = TracePathSplines.Num();
	const int32 NumSegmentsNeeded = TracePathLocations.Num() - 1;
	
	for (USplineMeshComponent *DynSpline : TracePathSplines)
	{
		DynSpline->SetVisibility(false);
	}
	
	for( int32 i=0; i < NumSegmentsNeeded; ++i)
	{
		USplineMeshComponent *DynSpline = nullptr;
		// add a segment if not in the cache
		if(i >= NumExistingSegments)
		{
			DynSpline = NewObject<USplineMeshComponent>(this);
			DynSpline->SetMobility(EComponentMobility::Movable);
			DynSpline->AttachToComponent(VRRoot, FAttachmentTransformRules::KeepRelativeTransform);
			DynSpline->SetStaticMesh(TraceMesh);
			DynSpline->SetMaterial(0, TraceMaterial);
			DynSpline->RegisterComponent();
			TracePathSplines.Add(DynSpline);
		}
		else
		{
			DynSpline = TracePathSplines[i];
		}

		FVector StartPoint, StartTangent, EndPoint, EndTangent;
		TeleportSplineComponent->GetLocationAndTangentAtSplinePoint(i, StartPoint, StartTangent, ESplineCoordinateSpace::Local);
		TeleportSplineComponent->GetLocationAndTangentAtSplinePoint(i + 1, EndPoint, EndTangent, ESplineCoordinateSpace::Local);
		DynSpline->SetStartAndEnd(StartPoint, StartTangent, EndPoint, EndTangent, false);
		DynSpline->SetVisibility(true);
		DynSpline->UpdateMesh();
	}
}

void AVRPawn::TraceMovement()
{
	if (!TeleportMarker)
	{
		UE_LOG(LogTemp, Warning, TEXT("Can't trace teleport marker, set it in you VR Character"));
		return;
	}

	const bool bCollision = TraceTeleportPath(TeleportHitResult, TracePathLocations);
	
	// show Destination marker at position if there is a hit location
	if(bCollision)
	{
		DrawTeleportSpline();
		
		// Draw the teleportation path
		DrawTeleportPath();
		
		UNavigationSystemV1* NavSys = UNavigationSystemV1::GetCurrent(GetWorld());
		FNavLocation NavLocation;
		bool bNavProj = NavSys->ProjectPointToNavigation(TeleportHitResult.Location, NavLocation, TeleportNavExtent);

		if(bNavProj)
		{
			if (!TeleportMarkerActor)
			{
				TeleportMarkerActor = GetWorld()->SpawnActor(TeleportMarker, &FTransform::Identity);
				if(TeleportRingSize != 1.0f)
				{
					TeleportMarkerActor->SetActorScale3D(FVector(TeleportRingSize, TeleportRingSize, 1.0f));
				}
				if(!TeleportMarkerActor){return;}
			}
			
			bIsTeleportValid = true;
			FVector TeleportLocation = NavLocation.Location;
			TeleportLocation.Z -= NavMeshOffset;
			TeleportMarkerActor->SetActorLocation(TeleportLocation);
		}
		else
		{
			bIsTeleportValid = false;
			RemoveTeleport();
		}
	}
	else
	{
		bIsTeleportValid = false;
		RemoveTeleport();
	}
}

void AVRPawn::RemoveTeleport()
{
	if(TeleportMarkerActor)
	{
		TeleportMarkerActor->Destroy();
		TeleportMarkerActor = nullptr;
	}
}

void AVRPawn::BeginTeleport()
{
	bIsTeleporting = true;
}

void AVRPawn::Teleport()
{
	// hide trace path
	for (USplineMeshComponent *DynSpline : TracePathSplines)
	{
		DynSpline->SetVisibility(false);
	}
	
	bIsTeleporting = false;
	if (!bIsTeleportValid){return;}
	
	FTimerHandle TimerHandle;
	GetWorldTimerManager().SetTimer(TimerHandle, this, &AVRPawn::EndTeleport, TeleportFadeSpeed);

	// Fade camera for blink
	UGameplayStatics::GetPlayerCameraManager(GetWorld(), 0)->StartCameraFade(
		0.0f,
		1.0f,
		TeleportFadeSpeed,
		FLinearColor::Black
		);
}

void AVRPawn::EndTeleport()
{
	// Teleport
	const FVector TeleportLocation = TeleportMarkerActor->GetActorLocation();

	RemoveTeleport();	
	SetActorLocation(TeleportLocation);

	
	// Fade camera to end blink
	UGameplayStatics::GetPlayerCameraManager(GetWorld(), 0)->StartCameraFade(
		1.0f,
		0.0f,
		TeleportFadeSpeed,
		FLinearColor::Black
		);
}

// MOVEMENT
void AVRPawn::CalculateMovementInput(float Scale)
{
	if(Scale == 0.0f)
	{
		MoveOffset = FVector::ZeroVector;
		return;
	}
	FVector Direction = CameraComponent->GetForwardVector();
	MoveOffset = Direction * MoveSpeed * Scale * GetWorld()->DeltaTimeSeconds;
}

void AVRPawn::Move()
{
	AddActorLocalOffset(MoveOffset, true);
}

void AVRPawn::GrabBegin(AHandController* MotionControllerComponent)
{
	MotionControllerComponent->Grab();
}

void AVRPawn::GrabLeftBegin()
{
	GrabBegin(LeftMotionController);
}

void AVRPawn::GrabRightBegin()
{
	GrabBegin(RightMotionController);
}

void AVRPawn::GrabEnd(AHandController* MotionController)
{
	MotionController->Release();
}

void AVRPawn::GrabLeftEnd()
{
	GrabEnd(LeftMotionController);
}

void AVRPawn::GrabRightEnd()
{
	GrabEnd(RightMotionController);
}

void AVRPawn::MoveForward(float AxisScale)
{
	if(!Controller){return;}
	if(AxisScale == 0){return;}
	if(!CameraComponent){return;}
	
	AddMovementInput(CameraComponent->GetForwardVector(), AxisScale);
}

void AVRPawn::MoveRight(float AxisScale)
{
	if(!Controller){return;}
	if(AxisScale == 0){return;}
	if(!CameraComponent){return;}

	AddMovementInput(CameraComponent->GetRightVector(), AxisScale);
}

