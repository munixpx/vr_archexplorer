﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Camera/CameraComponent.h"
#include "Components/SplineComponent.h"
#include "Components/SplineMeshComponent.h"
#include "GameFramework/Pawn.h"
#include "VR_ArchExplorer/Actors/HandController.h"
#include "VRPawn.generated.h"

UCLASS()
class VR_ARCHEXPLORER_API AVRPawn : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	AVRPawn();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

private:
	// COMPONENTS
	UPROPERTY(VisibleAnywhere)
	USceneComponent* VRRoot = nullptr;
	
	UPROPERTY(VisibleAnywhere)
	UCameraComponent* CameraComponent = nullptr;
	
	UPROPERTY(VisibleAnywhere)
	AHandController* LeftMotionController = nullptr;
	
	UPROPERTY(VisibleAnywhere)
	AHandController* RightMotionController = nullptr;

	UPROPERTY(VisibleAnywhere)
	USplineComponent* TeleportSplineComponent = nullptr;
	
	UPROPERTY(EditAnywhere)
	UBlueprintGeneratedClass* TeleportMarker = nullptr;

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<AHandController> MotionControllerClass;
	
	// TELEPORTATION
	bool TraceTeleportPath(FHitResult& OutHitResult, TArray<FVector>& OutPathLocations);
	void DrawTeleportSpline() const;
	void DrawTeleportPath();
	void TraceMovement();
	
	bool bIsTeleporting = false;
	bool bIsTeleportValid = false;
	
	void BeginTeleport();
	void Teleport();
	void EndTeleport();
	void RemoveTeleport();

	// Teleport
	UPROPERTY(EditAnywhere, Category="Teleport")
	float TeleportFadeSpeed = 0.5f;
	UPROPERTY(EditAnywhere, Category="Teleport")
	FVector TeleportNavExtent = FVector(100.0f, 100.0f, 100.0f);
	UPROPERTY(EditAnywhere, Category="Teleport")
	float NavMeshOffset = 8.0f;
	// Teleport trace
	UPROPERTY(EditAnywhere, Category="Teleport Trace")
	float TraceDebugRadius = 5;
	UPROPERTY(EditAnywhere, Category="Teleport Trace")
	float TraceSimTime = 10;
	UPROPERTY(EditAnywhere, Category="Teleport Trace")
	float TraceVelocity = 1000.0f;
	UPROPERTY(EditAnywhere, Category="Teleport Trace")
	float TraceArchAngleOffset = 15.0f;
	
	UPROPERTY(EditDefaultsOnly, Category="Teleport Trace")
	UStaticMesh* TraceMesh = nullptr;
	UPROPERTY(EditDefaultsOnly, Category="Teleport Trace")
	UMaterialInstance* TraceMaterial = nullptr;

	UPROPERTY(VisibleAnywhere)
	TArray<USplineMeshComponent*> TracePathSplines;
	
	UPROPERTY()
	FHitResult TeleportHitResult;
	UPROPERTY()
	TArray<FVector> TracePathLocations;
	
	// Teleport marker
	UPROPERTY()
	AActor* TeleportMarkerActor = nullptr;
	UPROPERTY(EditAnywhere, Category="Teleport")
	float TeleportRingSize = 1.0f;
	
	// MOVEMENTS
	void ResetVRRoot();
	virtual void MoveForward(float AxisScale);
	virtual void MoveRight(float AxisScale);

	UPROPERTY(EditAnywhere, Category="Movement")
	float MoveSpeed = 1000.0f;

	FVector MoveOffset;
	void CalculateMovementInput(float Scale);
	void Move();

	// Grabbing
	bool bIsGrabbing = false;
	
	void GrabBegin(AHandController*);
	void GrabLeftBegin();
	void GrabRightBegin();
	void GrabEnd(AHandController*);
	void GrabLeftEnd();
	void GrabRightEnd();
};

