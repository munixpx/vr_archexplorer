﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "MotionControllerComponent.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/Actor.h"
#include "HandController.generated.h"

UCLASS()
class VR_ARCHEXPLORER_API AHandController : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	AHandController();

	// Called every frame
	virtual void Tick(float DeltaTime) override;
	
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

private:

	UPROPERTY(EditAnywhere)
	UMotionControllerComponent* MotionControllerComponent = nullptr;

	// UPROPERTY(EditAnywhere)
	// UCapsuleComponent* CapsuleComponent = nullptr;

	UPROPERTY(EditDefaultsOnly)
	UHapticFeedbackEffect_Base* GrabHapticFeedBackEffect;
	
	UFUNCTION()
	void ActorBeginOverlap(AActor* OverlappedActor, AActor* OtherActor);
	UFUNCTION()
	void ActorEndOverlap(AActor* OverlappedActor, AActor* OtherActor );

	bool IsOverlappingClimbable() const;
	
	UPROPERTY()
	APawn* ParentPawn;
	UPROPERTY()
	APlayerController* PlayerController;
	
	bool bIsOnClimbable = false;
	bool bIsGrabbing = false;
	FVector GrabBeginLocation;
	
public:

	FName TAG_CLIMBABLE = TEXT("CLIMBABLE_TAG");
	
	void Grab();
	void Release();
	void SetTrackingSource(EControllerHand NewSource) const;
	void Climb();
};
