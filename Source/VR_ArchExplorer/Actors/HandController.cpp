﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "HandController.h"

#include "Kismet/GameplayStatics.h"


AHandController::AHandController()
{
	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	
	MotionControllerComponent = CreateDefaultSubobject<UMotionControllerComponent>(TEXT("MotionController"));
	SetRootComponent(MotionControllerComponent);
	
	// CapsuleComponent = CreateDefaultSubobject<UCapsuleComponent>(TEXT("CollisionCapsule"));
	// CapsuleComponent->SetupAttachment(MotionControllerComponent);
	
}

void AHandController::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	Climb();
}

// Called when the game starts or when spawned
void AHandController::BeginPlay()
{
	Super::BeginPlay();

	OnActorBeginOverlap.AddDynamic(this, &AHandController::ActorBeginOverlap);
	OnActorEndOverlap.AddDynamic(this, &AHandController::ActorEndOverlap);
}

void AHandController::Grab()
{
	bIsGrabbing = true;
	GrabBeginLocation = GetActorLocation();
}

void AHandController::Release()
{
	bIsGrabbing = false;
}

void AHandController::SetTrackingSource(EControllerHand NewSource) const
{
	MotionControllerComponent->SetTrackingSource(NewSource);
}

void AHandController::ActorBeginOverlap(AActor* OverlappedActor, AActor* OtherActor)
{
	const bool bIsOverlappingClimbable = IsOverlappingClimbable();

	if (!bIsOnClimbable && bIsOverlappingClimbable)
	{
		ParentPawn = Cast<APawn>(GetAttachParentActor());
		PlayerController = Cast<APlayerController>(ParentPawn->GetController());
		if(PlayerController && GrabHapticFeedBackEffect)
		{
			PlayerController->PlayHapticEffect(
				GrabHapticFeedBackEffect,
				MotionControllerComponent->GetTrackingSource());
		}
	}
	bIsOnClimbable = bIsOverlappingClimbable;
	
}

void AHandController::ActorEndOverlap(AActor* OverlappedActor, AActor* OtherActor)
{
	bIsOnClimbable = IsOverlappingClimbable();
}

bool AHandController::IsOverlappingClimbable() const
{
	TArray<AActor*> OverlappingActors;
	GetOverlappingActors(OverlappingActors);
	for(const AActor* OverlappingActor: OverlappingActors)
	{
		if(OverlappingActor->ActorHasTag(TAG_CLIMBABLE))
		{
			return true;
		}
	}
	return false;
}

void AHandController::Climb()
{
	if(!bIsGrabbing || !bIsOnClimbable){return;}
	// UE_LOG(LogTemp, Warning, TEXT("CLIMBLING!"));
	FVector Direction = GrabBeginLocation - GetActorLocation();
	ParentPawn = Cast<APawn>(GetAttachParentActor());
	ParentPawn->AddActorLocalOffset(Direction);
}




