// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "VR_ArchExplorerGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class VR_ARCHEXPLORER_API AVR_ArchExplorerGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
